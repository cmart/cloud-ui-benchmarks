# Lines of code

## Results

### OpenStack Horizon

```
    2797 text files.
    2788 unique files.
     417 files ignored.

github.com/AlDanial/cloc v 1.74  T=11.53 s (222.6 files/s, 52308.4 lines/s)
--------------------------------------------------------------------------------
Language                      files          blank        comment           code
--------------------------------------------------------------------------------
PO File                         142          90572           1151         273768
Python                          832          22072          20306          92524
JavaScript                      608           9225          20072          46940
HTML                            619           1173            169          14504
Sass                             97            987            548           4505
YAML                            238             57             25           2227
JSON                             24             81              0           1781
INI                               1             26              0            194
Bourne Shell                      4             32             34            117
Bourne Again Shell                1              1              0             14
--------------------------------------------------------------------------------
SUM:                           2566         124226          42305         436574
--------------------------------------------------------------------------------
```


### CyVerse Atmosphere

```
    2124 text files.
    2054 unique files.                                          
     303 files ignored.

github.com/AlDanial/cloc v 1.74  T=8.37 s (225.1 files/s, 20653.9 lines/s)
--------------------------------------------------------------------------------
Language                      files          blank        comment           code
--------------------------------------------------------------------------------
Python                          607          10806          11097          63080
JSX                             408           4278           1728          33217
JavaScript                      378           2966           6057          16358
YAML                            259           1107            887           5704
Markdown                         83           1337              0           4189
Sass                             31            500            113           2493
Cucumber                         15            167            172           1156
Bourne Again Shell               17            322            345            929
Bourne Shell                     33            280            146            867
HTML                             30            137             30            766
XML                               1            194              0            623
JSON                             10              2              0            340
Dockerfile                        3             28             18            142
CSS                               1             15              1             87
make                              1             27              0             53
INI                               3              0              0             43
Ruby                              3             12             14             43
SQL                               1              0              0             15
--------------------------------------------------------------------------------
SUM:                           1884          22178          20608         130105
--------------------------------------------------------------------------------
```


### Exosphere

```
      86 text files.
      86 unique files.                              
       6 files ignored.

github.com/AlDanial/cloc v 1.74  T=0.35 s (231.1 files/s, 72710.7 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Elm                             59           2205            305          16417
JSON                             4              0              0           5068
Markdown                         5            157              0            417
JavaScript                       4             45             33            179
CSS                              2             16              7            116
YAML                             2              9              2            104
HTML                             3             11              9             40
Dockerfile                       1              7              5             19
-------------------------------------------------------------------------------
SUM:                            80           2450            361          22360
-------------------------------------------------------------------------------
```


## Method

### Install `cloc` and `unzip`

MacOS:

```bash
brew install cloc unzip
```

Linux (Ubuntu/Debian):

```bash
apt install cloc unzip
```


### OpenStack Horizon

```bash
mkdir -p /tmp/cloud-ui-clocs/openstack/src
cd /tmp/cloud-ui-clocs/openstack

wget -O horizon.zip https://github.com/openstack/horizon/archive/master.zip

ls *.zip | xargs -L 1 unzip -d src
cloc src/
```


### CyVerse Atmosphere

```bash
mkdir -p /tmp/cloud-ui-clocs/cyverse/src
cd /tmp/cloud-ui-clocs/cyverse

wget -O atmosphere-ansible.zip https://github.com/cyverse/atmosphere-ansible/archive/master.zip
wget -O atmosphere.zip https://github.com/cyverse/atmosphere/archive/master.zip
wget -O chromogenic.zip https://github.com/cyverse/chromogenic/archive/master.zip
wget -O clank.zip https://github.com/cyverse/clank/archive/master.zip
wget -O django-cyverse-auth.zip https://github.com/cyverse/django-cyverse-auth/archive/master.zip
wget -O rtwo.zip https://github.com/cyverse/rtwo/archive/master.zip
wget -O troposphere.zip https://github.com/cyverse/troposphere/archive/master.zip

ls *.zip | xargs -L 1 unzip -d src
cloc src/
```


### Exosphere

```bash
mkdir -p /tmp/cloud-ui-clocs/exosphere/src
cd /tmp/cloud-ui-clocs/exosphere

wget -O exosphere.zip https://gitlab.com/exosphere/exosphere/-/archive/master/exosphere-master.zip

ls *.zip | xargs -L 1 unzip -d src
cloc src/
```
