import os
import pathlib

from behave import then, when, step
from behaving.personas.persona import persona_vars
from behaving.web.steps import i_press_xpath


@step('I pause for a breakpoint')
def i_pause_for_breakpoint(context):
    print('Stopping for a breakpoint')
    import pydevd_pycharm
    pydevd_pycharm.settrace('localhost', port=4567, stdoutToServer=True,
                            stderrToServer=True, suspend=True)


@step('I enable breakpoints')
def i_enable_breakpoints(context):
    print('Enabling breakpoints')
    import pydevd_pycharm
    pydevd_pycharm.settrace('localhost', port=4567, stdoutToServer=True,
                            stderrToServer=True, suspend=False)


@when('I go to Jetstream')
def i_go_to_jetstream(context):
    context.browser.visit(os.environ['ATMOURL'])


@then("I login to Jetstream")
def i_login_to_jetstream(context):
    context.browser.fill('login', os.environ.get('ATMOUSER'))
    context.browser.fill('password', os.environ.get('ATMOPASS'))


#
@step(u'I should see and press "{name}" within {timeout:d} seconds')
def i_should_see_and_press_within_timeout(context, name, timeout):
    assert context.browser.is_text_present(name,
                                           wait_time=timeout), u'Text not found'
    element_xpath = "//*[@id='%(name)s']|" \
                    "//*[@name='%(name)s']|" \
                    "//button[contains(string(), '%(name)s')]|" \
                    "//input[@type='button' and contains(string(), '%(name)s')]|" \
                    "//input[@type='button' and contains(@value, '%(name)s')]|" \
                    "//input[@type='submit' and contains(@value, '%(name)s')]|" \
                    "//a[contains(string(), '%(name)s')]" % {'name': name}
    assert context.browser.is_element_visible_by_xpath(element_xpath,
                                                       wait_time=10)
    element = context.browser.find_by_xpath(element_xpath)
    element.first.click()


# This step is a monster because behave-parallel does not have context.execute_steps implemented
# otherwise this function is about 10 lines
@step(u'I create project "{project}" if necessary')
def i_create_project(context, project):
    # see and press "Projects"
    name = 'Projects'
    assert context.browser.is_text_present(name,
                                           wait_time=10), u'Text not found'
    element = context.browser.find_by_xpath(
        ("//*[@id='%(name)s']|"
         "//*[@name='%(name)s']|"
         "//button[contains(string(), '%(name)s')]|"
         "//input[@type='button' and contains(string(), '%(name)s')]|"
         "//input[@type='button' and contains(@value, '%(name)s')]|"
         "//input[@type='submit' and contains(@value, '%(name)s')]|"
         "//a[contains(string(), '%(name)s')]") % {'name': name})
    assert element, u'Element not found'
    element.first.click()

    if not context.browser.is_element_present_by_xpath(
            "//h2[contains(., '%s')]" % project):
        # Then I should see and press "Create New Project" within 10 seconds
        name = "Create New Project"
        assert context.browser.is_text_present(name.upper(),
                                               wait_time=10), u'Text not found'
        element = context.browser.find_by_xpath(
            ("//*[@id='%(name)s']|"
             "//*[@name='%(name)s']|"
             "//button[contains(string(), '%(name)s')]|"
             "//input[@type='button' and contains(string(), '%(name)s')]|"
             "//input[@type='button' and contains(@value, '%(name)s')]|"
             "//input[@type='submit' and contains(@value, '%(name)s')]|"
             "//a[contains(string(), '%(name)s')]") % {'name': name})
        assert element, u'Element not found'
        element.first.click()

        # Then I should see "Project Name" within 10 seconds
        name = "Create Project"
        assert context.browser.is_text_present(name,
                                               wait_time=10), u'Text not found'

        # And I type slowly "%s" to "0" index of class "form-control"
        assert context.browser.is_element_visible_by_xpath(
            "//div[@class='modal-body']//input[@type='text']",
            wait_time=10)
        element = context.browser.find_by_xpath(
            "//div[@class='modal-body']//input[@type='text']")
        assert element, u'Element not found'
        for key in element.first.type(project, slowly=True):
            assert key

        # And I type slowly "%s" to "1" index of class "form-control"
        element = context.browser.find_by_xpath(
            "//div[@class='modal-body']//textarea[@name='project-description']")
        assert element, u'Element not found'
        for key in element.first.type(project, slowly=True):
            assert key

        # And I press "submitCreateProject"
        name = "submitCreateProject"
        element = context.browser.find_by_xpath(
            ("//*[@id='%(name)s']|"
             "//*[@name='%(name)s']|"
             "//button[contains(string(), '%(name)s')]|"
             "//input[@type='button' and contains(string(), '%(name)s')]|"
             "//input[@type='button' and contains(@value, '%(name)s')]|"
             "//input[@type='submit' and contains(@value, '%(name)s')]|"
             "//a[contains(string(), '%(name)s')]") % {'name': name})
        assert element, u'Element not found'
        element.first.click()

        # Then I should see "%s" within 10 seconds
        assert context.browser.is_text_present(project,
                                               wait_time=10), u'Text not found'


@when("I go to Horizon")
def step_impl(context):
    context.browser.visit(os.environ['HORIZON_IUURL'])


@then("I login to Horizon")
def i_login_to_jetstream(context):
    context.browser.fill('domain', 'TACC')
    context.browser.fill('username', os.environ.get('HORIZONUSER'))
    context.browser.fill('password', os.environ.get('HORIZONPASS'))


@step(u'I fill in instance name with "{value}"')
# @persona_vars
def i_fill_in_instance_name(context, value):
    field = context.browser.find_by_id('name').first
    field.value = value


@then("I press the source upload button")
def source_upload(context):
    xpath_source = "/html/body/div[1]/div/div/wizard/div/div[2]/div[2]/div[2]/div[2]/ng-include/div/div/transfer-table/div/div[2]/div[2]/available/hz-magic-search-context/table/tbody/tr[293]/td[7]/action-list/button/ng-transclude/span"
    i_press_xpath(context, xpath_source)


### Exosphere-specific steps below

@when('I go to Exosphere')
def i_go_to_exosphere(context):
    exosphere_url = context.config.userdata.get('EXOSPHERE_BASE_URL',
                                                'https://try.exosphere.app/exosphere')
    context.browser.visit(exosphere_url)


def find_by_label(context, label, element_type, wait_time=None):
    return context.browser.find_by_xpath(
        xpath=f"//label[contains(string(),'{label}')]//{element_type}",
        wait_time=wait_time
    )


def find_input_by_label(context, label, wait_time=None):
    return find_by_label(
        context=context,
        label=label,
        element_type='input',
        wait_time=wait_time)


@step(u'I fill input labeled "{label}" with "{value}"')
@persona_vars
def i_fill_input_labeled(context, label, value):
    element = find_input_by_label(context, label).first
    element.fill(value)


def find_element_with_role_and_label(context, role, label, wait_time=None):
    return context.browser.find_by_xpath(
        xpath=f"//div[@role='{role}']//div[contains(string(), '{label}')]",
        wait_time=wait_time
    )


def find_button_with_label(context, label, wait_time=None):
    return find_element_with_role_and_label(
        context=context,
        role='button',
        label=label,
        wait_time=wait_time
    )


def find_checkbox_with_label(context, label, wait_time=None):
    return context.browser.find_by_xpath(
        xpath=f"//label[contains(string(), '{label}')]//div[@role='checkbox']",
        wait_time=wait_time)


def find_radio_with_label(context, label, wait_time=None):
    return context.browser.find_by_xpath(
        xpath=f"//div[@role='radio' and contains(string(), '{label}')]",
        wait_time=wait_time)


@step(u'I click the "{label}" button')
@persona_vars
def i_press_label_button(context, label):
    all_elements = find_button_with_label(context, label)
    case_insensitive_elements = [e for e in all_elements if
                                 str.upper(e.text) == str.upper(label)]
    case_sensitive_elements = [e for e in all_elements if e.text == label]
    elements = case_sensitive_elements or case_insensitive_elements
    if elements:
        element = elements[0]
    else:
        element = all_elements.first
    element.click()


@step(u'I click the last "{label}" button')
@persona_vars
def i_press_last_label_button(context, label):
    all_elements = find_button_with_label(context, label)
    case_insensitive_elements = [e for e in all_elements if
                                 str.upper(e.text) == str.upper(label)]
    case_sensitive_elements = [e for e in all_elements if e.text == label]
    elements = case_sensitive_elements or case_insensitive_elements
    if elements:
        element = elements[-1]
    else:
        element = all_elements.last
    element.click()


@step(u'I press the last element with xpath "{xpath}"')
@persona_vars
def i_press_last_xpath(context, xpath):
    button = context.browser.find_by_xpath(xpath)
    assert button, u"Element not found"
    button.last.click()


@step(u'I click the "{label}" checkbox')
@persona_vars
def i_press_label_checkbox(context, label):
    element = find_checkbox_with_label(context, label).first
    element.click()


@step(u'I click the "{label}" radio button')
@persona_vars
def i_press_label_radiobutton(context, label):
    element = find_radio_with_label(context, label).first
    element.click()


@step("I enter TACC credentials")
@persona_vars
def i_login_to_exosphere(context):
    taccusername = os.environ.get('taccusername')
    taccpass = os.environ.get('taccpass')
    context.execute_steps(f"""
    Then I fill input labeled "TACC Username" with "{taccusername}"
    And I fill input labeled "TACC Password" with "{taccpass}"
    """)


@step('I save the "{local_storage_item}" item in browser local storage')
@persona_vars
def i_save_local_storage(context, local_storage_item):
    local_storage_item_content = context.browser.evaluate_script(
        f"localStorage.getItem('{local_storage_item}')")
    with open(f'/tmp/{local_storage_item}', 'w') as temporary_file:
        temporary_file.write(local_storage_item_content)


@step('I load the "{local_storage_item}" item in browser local storage')
@persona_vars
def i_load_local_storage(context, local_storage_item):
    with open(f'/tmp/{local_storage_item}', 'r') as temporary_file:
        local_storage_item_content = temporary_file.read()
        context.browser.evaluate_script(
            f"localStorage.setItem('{local_storage_item}', '{local_storage_item_content}')")
        context.browser.reload()


@step('I delete "{local_storage_item}" browser local storage item file')
@persona_vars
def i_load_local_storage(context, local_storage_item):
    file_path = pathlib.Path(f'/tmp/{local_storage_item}')
    file_path.unlink(missing_ok=True)
