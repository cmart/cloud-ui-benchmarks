Feature: Text presence

    Background:
        Given browser "Firefox"

    Scenario: Create an instance
        When I go to Horizon
        Then I login to Horizon
        Then I should see and press "Sign In" within 5 seconds
        Then I press "Project"
        Then I press "Compute"
        Then I press "Instances"
        Then I should see and press "Launch Instance" within 45 seconds
        Then I fill in instance name with "BDD-Test"
        Then I should see and press "Source" within 5 seconds
        And I wait for 20 seconds
        Then I press the source upload button
        Then I wait for 5 seconds
        Then I press the element with xpath "/html/body/div[1]/div/div/wizard/div/div[2]/div[2]/div[2]/div[2]/ng-include/div/div/transfer-table/div/div[1]/div[2]/allocated/table/tbody/tr[1]/td[2]"
        Then I wait for 3 seconds
        Then I should see and press "Flavor" within 5 seconds
        Then I wait for 5 seconds
        Then I press the element with xpath "/html/body/div[1]/div/div/wizard/div/div[2]/div[2]/div[2]/div[3]/ng-include/div/transfer-table/div/div[2]/div[2]/available/hz-magic-search-context/table/tbody/tr[5]/td[9]/action-list/button/ng-transclude/span"
        Then I wait for 3 seconds
        Then I should see and press "Network" within 5 seconds
        Then I wait for 3 seconds
        Then I press the element with xpath "/html/body/div[1]/div/div/wizard/div/div[2]/div[2]/div[2]/div[4]/ng-include/div/transfer-table/div/div[2]/div[2]/available/hz-magic-search-context/table/tbody/tr[23]/td[7]/action-list/button/ng-transclude/span"
        Then I wait for 3 seconds
        Then I should see and press "Security Groups" within 5 seconds
        Then I wait for 3 seconds
        Then I press the element with xpath "/html/body/div[1]/div/div/wizard/div/div[2]/div[2]/div[2]/div[6]/ng-include/div/transfer-table/div/div[2]/div[2]/available/hz-magic-search-context/table/tbody/tr[33]/td[4]/action-list/button/ng-transclude/span"
        Then I press the element with xpath "/html/body/div[1]/div/div/wizard/div/div[2]/div[2]/div[2]/div[6]/ng-include/div/transfer-table/div/div[1]/span"
        Then I wait for 3 seconds
      #  Then I should see and press "Key Pair" within 5 seconds
        Then I should see and press "Launch Instance" within 5 seconds
        And I wait for 20 seconds
        Then I press "Instances"
        And I wait for 60 seconds
        Then I press "BDD-Test"
        And I wait for 60 seconds
        Then I press the element with xpath "//*[@id="content_body"]/div[1]/div/div/div[2]/div/div[2]/form/div/a[2]"
        Then I press "Associate Floating IP"
         Then I wait for 3 seconds
        And I press the element with xpath "//*[@id="ip_association__associateipaction"]/div/div[1]/div[1]/div/div/div/ul/li[2]"
       # Then I wait for 3 seconds


        Then I wait for 15 seconds